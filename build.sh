#!/usr/bin/env sh

set -xe

gcc -Wall -Wextra -O3 -o mirrorworld ./main.c -lSDL2 -lm
gcc -Wall -Wextra -o mirrorworld.debug ./main.c -Ddebug -lSDL2 -lSDL2_ttf -lm
