#ifndef INTERSECT_H_
#define INTERSECT_H_

typedef struct {
    float x, y;
} Vec;

typedef struct {
    Vec a, b;
} Seg;

typedef enum {
    CT_REGULAR,
    CT_RAY,
    CT_LINE,
    CT_TWOLINES,
} CrossType;

Vec vec(float x, float y);
float vdot(Vec v1, Vec v2);
float vcross(Vec v1, Vec v2);
Vec vsub(Vec v1, Vec v2);
Vec vadd(Vec v1, Vec v2);
Vec vscale(Vec v1, float t);
Vec vnormal(Vec v);
Seg seg(Vec a, Vec b);
Seg seg_from_absolute(Vec a, Vec b);
Vec endp(Seg s);
bool v_is_valid_scale(Vec v, float t);
bool intersect(Seg s1, Seg s2, float* scale, CrossType ct);

#ifdef INTERSECT_IMPLEMENTATION

// The minimum t at which it counts as an intersection
//
// This is to prevent a reflection from intersecting with the line it reflected from.
//
// TODO: Should MIN_LEN be replaced with an id system?
#define MIN_LEN 2.0f

Vec vec(float x, float y) {
    Vec v;
    v.x = x;
    v.y = y;
    return v;
}

float vdot(Vec v1, Vec v2) {
    return v1.x * v2.x + v1.y * v2.y;
}

float vcross(Vec v1, Vec v2) {
    return v1.x * v2.y - v1.y * v2.x;
}

Vec vsub(Vec v1, Vec v2) {
    return vec(v1.x - v2.x, v1.y - v2.y);
}

Vec vadd(Vec v1, Vec v2) {
    return vec(v1.x + v2.x, v1.y + v2.y);
}

Vec vscale(Vec v1, float t) {
    return vec(v1.x * t, v1.y * t);
}

Vec vnormal(Vec v) {
    if (v.x < 0) {
        return vec(-v.y, v.x);
    } else if (v.x > 0) {
        return vec(v.y, -v.x);
    } else {
        return vec(-v.y, -v.x);
    }
}

Seg seg(Vec a, Vec b) {
    Seg s;
    s.a = a;
    s.b = b;
    return s;
}

Seg seg_from_absolute(Vec a, Vec b) {
    return seg(a, vsub(b, a));
}

Vec endp(Seg s) {
    return vadd(s.a, s.b);
}

bool v_is_valid_scale(Vec v, float t) {
    return (t * t * (v.x * v.x + v.y * v.y) > MIN_LEN * MIN_LEN);
}

bool intersect(Seg s1, Seg s2, float* scale, CrossType ct) {
    Vec q = s2.a, p = s1.a, r = s1.b, s = s2.b;

    float qpr = vcross(vsub(q, p), r);

    float t = vcross(vsub(q, p), s) / vcross(r, s);

    float u = qpr / vcross(r, s);

    float rs = vcross(r, s);

    if (qpr == 0.0 && rs == 0.0) {
        // Lines are collinear

        float t0 = vdot(vsub(q, p), r) / vdot(r, r);
        float t1 = t0 + vdot(s, r) / vdot(r, r);
        if (vdot(s, r) < 0) {
            float f = t0;
            t0 = t1;
            t1 = f;
        }

        if (0.0 <= t0 && t0 <= 1 && v_is_valid_scale(r, t0)) {
            *scale = t0;
            return true;
        }
        if (t0 < 0 && t1 > 0) {
            *scale = 0.0;
            return true;
        }
        if (t0 > 1 && t1 < 1) {
            *scale = 1.0;
            return true;
        }
        return false;
    } else if (rs == 0 && qpr != 0) {
        return false;
    } else if (rs != 0 ) {
        switch (ct) {
            case CT_REGULAR: {
                if (0 < t && t < 1 && 0 < u && u < 1 && v_is_valid_scale(r, t)) {
                    *scale = t;
                    return true;
                }
                return false;
            }; break;
            case CT_RAY: {
                if (0 < t && 0 < u && u < 1 && v_is_valid_scale(r, t)) {
                    *scale = t;
                    return true;
                }
                return false;
            }; break;
            case CT_LINE: {
                if (0 < u && u < 1) {
                    *scale = t;
                    return true;
                }
                return false;
            }; break;
            case CT_TWOLINES: {
                *scale = t;
                return true;
            }; break;
        }
    }

    return false;
}

#endif // INTERSECT_IMPLEMENTATION

#endif // INTERSECT_H_
