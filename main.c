#include <SDL2/SDL_events.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_video.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include <assert.h>
#include <math.h>
#ifdef debug
#include <SDL2/SDL_ttf.h>
#endif //debug

#define INTERSECT_IMPLEMENTATION
#include "intersect.h"

#define UNPACK_COLOUR(c) (c >> 8 * 3) & 0xFF, (c >> 8 * 2) & 0xFF, (c >> 8 * 1) & 0xFF, (c >> 8 * 0) & 0xFF
#define BACKGROUND 0x181818FF
#define WALL 0xAAAAAAFF

void sc(int code, char* action) {
    if (code != 0) {
        fprintf(stderr, "ERROR %s: %s! Aborting!\n", action != NULL ? action : "", SDL_GetError());
        exit(1);
    }
}

void* scp(void* res, char* action) {
    if (res == NULL) {
        fprintf(stderr, "ERROR %s: %s! Aborting!\n", action != NULL ? action : "", SDL_GetError());
        exit(1);
    }
    return res;
}

void clear(SDL_Renderer* r) {
    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(BACKGROUND));
    SDL_RenderClear(r);
}

#define POOL_SIZE 100000

typedef struct {
    Seg data[POOL_SIZE];
    int used;
} Pool;

void pool_push(Pool* p, Seg s) {
    if (p->used < POOL_SIZE - 1) {
        p->data[p->used] = s;
        p->used++;
    }
}

void pool_pop(Pool* p) {
    if (p->used > 0)
        p->used--;
}

// These are the scary *global variables!*
// Don't let your CS teacher see it or they might have an aneurysm!!
Pool blocks = {0};
Pool rays = {0};
Seg deck = {0};
Vec mouse_pos = {0};
bool deck_full = false;
bool uncoupled = false;
bool needs_triangle = false;
int width = 800;
int height = 600;

#ifdef debug
TTF_Font* font;
#endif

void draw_seg(SDL_Renderer* r, Seg s) {
    Vec endpoint = vadd(s.a, s.b);
    sc(SDL_RenderDrawLineF(r, s.a.x, s.a.y, endpoint.x, endpoint.y), "drawing a line");
}

#define COLOR_SCALE 200
#define TRIANGLE_LEN 20.0f

void render(SDL_Renderer* r) {
    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(WALL));
    for (int i = 0; i < blocks.used; ++i) {
        draw_seg(r, blocks.data[i]);
    }

    for (int i = 0; i < rays.used; ++i) {
        int t = (int) (COLOR_SCALE * (float) i / (float) rays.used);
        SDL_SetRenderDrawColor(r, 0, COLOR_SCALE - t, t, 255);
        if (i + 1 == rays.used)
            SDL_SetRenderDrawColor(r, 255, 255, 255, 255);
        draw_seg(r, rays.data[i]);
    }

    if (needs_triangle) {
        Seg final_ray = rays.data[rays.used - 1];
        Vec endp = sendp(final_ray);
        Vec norm = vscale_to_len(vnormal(final_ray.b), TRIANGLE_LEN / 2);
        Vec p1, p2, p3;
        p1 = vsub(endp, norm);
        p2 = vadd(endp, norm);
        p3 = vadd(endp, vscale_to_len(final_ray.b, TRIANGLE_LEN));

        SDL_Vertex v[3] = {0};
        SDL_Color white = {UNPACK_COLOUR(0xFFFFFFFF)};

        v[0].color = white;
        v[1].color = white;
        v[2].color = white;

        v[0].position = v_to_fpoint(p1);
        v[1].position = v_to_fpoint(p2);
        v[2].position = v_to_fpoint(p3);

        // Leave tex_coords null

        sc(SDL_RenderGeometry(r, NULL, v, sizeof(v) / sizeof(v[0]), NULL, 0), "rendering triangle");
    }

    if (deck_full) {
        SDL_SetRenderDrawColor(r, UNPACK_COLOUR(0x535353FF));
        draw_seg(r, deck);
    }

    #ifdef debug
    SDL_Color c = {255, 255, 255, 255};
    char raw[256] = {0};
    snprintf(raw, sizeof(raw), "[DEBUG] rays: %i blocks: %i", rays.used, blocks.used);
    SDL_Surface* surf_msg = scp(TTF_RenderText_Solid(font, raw, c), "creating surface");
    SDL_Texture* msg = scp(SDL_CreateTextureFromSurface(r, surf_msg), "creating texture");
    SDL_Rect rect = (SDL_Rect) {
        .x = 0,
        .y = height - 120,
        .h = 45,
        .w = 300
    };
    sc(SDL_RenderCopy(r, msg, NULL, &rect), "writing text");
    SDL_FreeSurface(surf_msg);
    SDL_DestroyTexture(msg);
    #endif

}

bool reflect(Seg l, Seg b, Seg* reflection, CrossType ct) {
    float scale = {0};
    bool hits = intersect(l, b, &scale, ct);
    if (hits) {
        Vec cross = vadd(l.a, vscale(l.b, scale));
        l.b = vsub(cross, l.a);
        Seg normal, to_normal, from_normal;
        normal.a = cross;
        normal.b = vnormal(b.b);
        to_normal.a = l.a;
        to_normal.b = b.b;
        float normscale = 1;
        assert(intersect(to_normal, normal, &normscale, CT_TWOLINES));
        from_normal.a = vadd(to_normal.a, vscale(to_normal.b, normscale));
        to_normal.b = vsub(from_normal.a, to_normal.a);
        from_normal.b = to_normal.b;
        reflection->a = cross;
        reflection->b = vsub(vadd(from_normal.a, from_normal.b), reflection->a);
    }
    return hits;
}

void update_deck() {
    deck = seg_from_absolute(deck.a, mouse_pos);
}

void scale_last_ray() {
    #define NORMAL_END_LEN 300.0f
    Vec* v = &rays.data[rays.used - 1].b;
    *v = vscale_to_len(*v, NORMAL_END_LEN);
}

void update_rays(float x, float y) {
    if (!uncoupled)
        rays.data[0].b = vsub(vec(x, y), rays.data[0].a);

    rays.used = 1;
    bool search_for_hits = true;

    while (rays.used < POOL_SIZE && search_for_hits) {

        search_for_hits = false;
        float scale = 0.0;
        // TODO: There is a bug here: if a ray hits precisely at the overlap of two blocks,
        // it will bounce only off of an arbitrary one of those blocks and seemingly ignore
        // the other. This should be fixed.
        float smallest_scale = INFINITY;
        int idx = -1;

        for (int i = 0; i < blocks.used; ++i) {
            if (intersect(rays.data[rays.used - 1], blocks.data[i], &scale, CT_RAY)) {
                search_for_hits = true;
                if (scale < smallest_scale) {
                    smallest_scale = scale;
                    idx = i;
                }
            }
        }

        if (deck_full) {
            if (intersect(rays.data[rays.used - 1], deck, &scale, CT_RAY)) {
                search_for_hits = true;
                if (scale < smallest_scale) {
                    smallest_scale = scale;
                    idx = -1;
                }
            }
        }

        if (search_for_hits) {
            Seg found_block = idx == -1 ? deck : blocks.data[idx];

            if (!reflect(rays.data[rays.used - 1], found_block, &rays.data[rays.used], CT_RAY)) {
                Seg r = rays.data[rays.used - 1];
                Seg b = found_block;
                fprintf(stderr, "[ERROR]: ray: (%f, %f) -> (%f, %f)\n", r.a.x, r.a.y, r.b.x, r.b.y);
                fprintf(stderr, "[ERROR]: block (%i): (%f, %f) -> (%f, %f)\n", idx, b.a.x, b.a.y, b.b.x, b.b.y);
            }

            rays.data[rays.used - 1].b = vscale(rays.data[rays.used - 1].b, smallest_scale);
            rays.used++;
        }
    }

    if (rays.used == POOL_SIZE) {
        // All the rays have been used

        bool found_hit = false;
        float scale = 0.0;
        // TODO: There is a bug here: if a ray hits precisely at the overlap of two blocks,
        // it will bounce only off of an arbitrary one of those blocks and seemingly ignore
        // the other. This should be fixed.
        float smallest_scale = INFINITY;

        for (int i = 0; i < blocks.used; ++i) {
            if (intersect(rays.data[rays.used - 1], blocks.data[i], &scale, CT_RAY)) {
                found_hit = true;
                if (scale < smallest_scale) {
                    smallest_scale = scale;
                }
            }
        }

        if (deck_full) {
            if (intersect(rays.data[rays.used - 1], deck, &scale, CT_RAY)) {
                found_hit = true;
                if (scale < smallest_scale) {
                    smallest_scale = scale;
                }
            }
        }

        if (found_hit) {
            rays.data[rays.used - 1].b = vscale(rays.data[rays.used - 1].b, smallest_scale);
            needs_triangle = false;
        } else {
            needs_triangle = true;
            scale_last_ray();
        }
    } else {
        needs_triangle = true;
        scale_last_ray();
    }
}

int main() {
    pool_push(&rays, seg(vec((float) width / 2, (float) height / 2), vec(0.0f, 0.0f)));

    pool_push(
        &blocks,
        seg(
            vec(430, 200),
            vec(150, 200)
        )
    );

    pool_push(
        &blocks,
        seg(
            vec(410 + 150, 420),
            vec(-320, 0)
        )
    );

    pool_push(
        &blocks,
        seg(
            vec(370, 200),
            vec(-150, 200)
        )
    );

    sc(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS), "Initiating SDL");

    #ifdef debug
    TTF_Init();
    font = scp(TTF_OpenFont("/usr/share/fonts/liberation-mono/LiberationMono-Regular.ttf", 15), "opening font");
    #endif

    SDL_Window* w = scp(SDL_CreateWindow("Mirrorworld", 0, 0, width, height, SDL_WINDOW_RESIZABLE), "creating window");
    SDL_Renderer* r = scp(SDL_CreateRenderer(w, -1, SDL_RENDERER_ACCELERATED), "creating renderer");

    clear(r);

    bool running = true;

    SDL_Event e;

    while(running) {
        while(SDL_PollEvent(&e)) {
            switch(e.type) {
                case SDL_KEYUP: {
                    switch (e.key.keysym.scancode) {
                        case SDL_SCANCODE_Q: {
                            running = false;
                        }; break;
                        case SDL_SCANCODE_ESCAPE: {
                            running = false;
                        }; break;
                        case SDL_SCANCODE_BACKSPACE: {
                            pool_pop(&blocks);
                            update_rays(mouse_pos.x, mouse_pos.y);
                        }; break;
                        case SDL_SCANCODE_SPACE: {
                            uncoupled = !uncoupled;
                        }; break;
                        default:
                            break;
                    }
                };
                    break;
                case SDL_WINDOWEVENT: {
                    switch (e.window.event) {
                        case SDL_WINDOWEVENT_CLOSE: {
                            running = false;
                        }; break;
                        case SDL_WINDOWEVENT_RESIZED: {
                            float dx = (float) (e.window.data1 - width) / 2;
                            float dy = (float) (e.window.data2 - height) / 2;
                            for (int i = 0; i < rays.used; ++i) {
                                rays.data[i].a.x += dx;
                                rays.data[i].a.y += dy;
                            }
                            for (int i = 0; i < blocks.used; ++i) {
                                blocks.data[i].a.x += dx;
                                blocks.data[i].a.y += dy;
                            }
                            width = e.window.data1;
                            height = e.window.data2;
                            Vec end = sendp(rays.data[0]);
                            update_rays(end.x, end.y);
                        }; break;
                        default:
                            break;
                    }
                }; break;
                case SDL_MOUSEBUTTONUP: {
                    #define LEFT_CLICK 1
                    #define RIGHT_CLICK 3
                    mouse_pos = vec((float) e.button.x, (float) e.button.y);
                    if (e.button.button == LEFT_CLICK) {
                        rays.data[0].a = mouse_pos;
                        update_rays((float) e.button.x, (float) e.button.y);
                    } else if (e.button.button == RIGHT_CLICK) {
                        if (deck_full) {
                            pool_push(&blocks, deck);
                            deck_full = false;
                            update_rays((float) e.button.x, (float) e.button.y);
                        }
                    }
                }; break;
                case SDL_MOUSEBUTTONDOWN: {
                    if (e.button.button == RIGHT_CLICK && !deck_full) {
                        deck.a = mouse_pos;
                        deck_full = true;
                        update_deck();
                    }
                }; break;
                case SDL_MOUSEMOTION: {
                    mouse_pos = vec((float) e.button.x, (float) e.button.y);
                    if (deck_full)
                        update_deck();
                    update_rays(mouse_pos.x, mouse_pos.y);
                }; break;
                default:
                    break;
            }
        }
        clear(r);
        render(r);
        SDL_RenderPresent(r);
    }

    return 0;
}
